export abstract class PasswordStrategy {
  public constructor() {
    this.hashPassword = this.hashPassword.bind(this);
    this.comparePassword = this.comparePassword.bind(this);
    this.hashPasswordSync = this.hashPasswordSync.bind(this);
    this.comparePasswordSync = this.comparePasswordSync.bind(this);
  }

  abstract hashPassword(password: string): Promise<string>;

  abstract comparePassword(
    password: string,
    passwordHash: string
  ): Promise<boolean>;

  abstract hashPasswordSync(password: string): string;

  abstract comparePasswordSync(password: string, passwordHash: string): boolean;
}
