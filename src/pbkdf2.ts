import { pbkdf2, pbkdf2Sync, randomBytes } from "crypto";
import { promisify } from "util";
import { guardPositiveInteger, guardString } from "./utils";

import { PasswordStrategy } from "./type";

export type Options = {
  hashBytes?: number;
  saltBytes?: number;
  iterations?: number;
  digest?: string;
};
export class PBKDF2Strategy extends PasswordStrategy {
  protected hashBytes: number;
  protected saltBytes: number;
  protected iterations: number;
  protected digest: string;
  public constructor(options: Options = {}) {
    const {
      hashBytes = 32,
      saltBytes = 16,
      iterations = 65535,
      digest = "whirlpool"
    } = options;
    super();
    guardPositiveInteger(hashBytes, "hashBytes");
    guardPositiveInteger(saltBytes, "saltBytes");
    guardPositiveInteger(iterations, "iterations");
    guardString(digest, "digest");
    this.hashBytes = hashBytes;
    this.saltBytes = saltBytes;
    this.iterations = iterations;
    this.digest = digest;
  }

  public async hashPassword(password: string): Promise<string> {
    const salt = await promisify(randomBytes)(this.saltBytes);
    const hash = await promisify(pbkdf2)(
      password,
      salt,
      this.iterations,
      this.hashBytes,
      this.digest
    );
    const combined = Buffer.alloc(hash.length + salt.length + 8);
    combined.writeUInt32BE(salt.length, 0);
    combined.writeUInt32BE(this.iterations, 4);
    salt.copy(combined, 8);
    hash.copy(combined, salt.length + 8);
    return combined.toString("base64");
  }

  public async comparePassword(
    password: string,
    passwordHash: string
  ): Promise<boolean> {
    const buffer = Buffer.from(passwordHash, "base64");
    const saltBytes = buffer.readUInt32BE(0);
    const hashBytes = buffer.length - saltBytes - 8;
    const iterations = buffer.readUInt32BE(4);
    const salt = buffer.slice(8, saltBytes + 8);
    const hash = buffer.toString("binary", saltBytes + 8);
    const digest = this.digest;
    const verify = await promisify(pbkdf2)(
      password,
      salt,
      iterations,
      hashBytes,
      digest
    );
    return verify.toString("binary") === hash;
  }

  public hashPasswordSync(password: string): string {
    const salt = randomBytes(this.saltBytes);
    const hash = pbkdf2Sync(
      password,
      salt,
      this.iterations,
      this.hashBytes,
      this.digest
    );
    const combined = Buffer.alloc(hash.length + salt.length + 8);
    combined.writeUInt32BE(salt.length, 0);
    combined.writeUInt32BE(this.iterations, 4);
    salt.copy(combined, 8);
    hash.copy(combined, salt.length + 8);
    return combined.toString("base64");
  }

  public comparePasswordSync(password: string, passwordHash: string): boolean {
    const buffer = Buffer.from(passwordHash, "base64");
    const saltBytes = buffer.readUInt32BE(0);
    const hashBytes = buffer.length - saltBytes - 8;
    const iterations = buffer.readUInt32BE(4);
    const salt = buffer.slice(8, saltBytes + 8);
    const hash = buffer.toString("binary", saltBytes + 8);
    const digest = this.digest;
    const verify = pbkdf2Sync(password, salt, iterations, hashBytes, digest);
    return verify.toString("binary") === hash;
  }
}
