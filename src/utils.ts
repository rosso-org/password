import _ from "lodash";

export function isPositiveInteger(value: any): value is number {
  return _.isInteger(value) && value > 0;
}

export function guardPositiveInteger(value: any, name: string): void {
  if (!isPositiveInteger(value)) {
    throw new Error(`${name} must be positive integer`);
  }
}

export function guardString(value: any, name: string): void {
  if (!_.isString(value)) {
    throw new Error(`${name} must be string`);
  }
}
