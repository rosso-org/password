import bcrypt from "bcrypt";

import { PasswordStrategy } from "./type";
import { guardPositiveInteger } from "./utils";

export type Options = {
  saltRounds?: number;
};

export class BcryptStrategy extends PasswordStrategy {
  protected saltRounds: number;
  public constructor(options: Options = {}) {
    const { saltRounds = 10 } = options;
    super();
    guardPositiveInteger(saltRounds, "saltRounds");
    this.saltRounds = saltRounds;
  }

  public hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, this.saltRounds);
  }

  public comparePassword(
    password: string,
    passwordHash: string
  ): Promise<boolean> {
    return bcrypt.compare(password, passwordHash);
  }

  public hashPasswordSync(password: string): string {
    return bcrypt.hashSync(password, this.saltRounds);
  }

  public comparePasswordSync(password: string, passwordHash: string): boolean {
    return bcrypt.compareSync(password, passwordHash);
  }
}
