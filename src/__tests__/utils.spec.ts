import { guardPositiveInteger, guardString, isPositiveInteger } from "../utils";

describe("utils", () => {
  test("isPositiveInteger", () => {
    expect(isPositiveInteger(1)).toBe(true);
    expect(isPositiveInteger(0)).toBe(false);
    expect(isPositiveInteger(-1)).toBe(false);
    expect(isPositiveInteger("1")).toBe(false);
  });
  test("guardPositiveInteger", () => {
    expect(() => guardPositiveInteger(1, "value")).not.toThrow();
    expect(() => guardPositiveInteger(0, "value")).toThrow();
    expect(() => guardPositiveInteger(-1, "value")).toThrow();
    expect(() => guardPositiveInteger("1", "value")).toThrow();
  });
  test("guardString", () => {
    expect(() => guardString("", "value")).not.toThrow();
    expect(() => guardString(0, "value")).toThrow();
  });
});
