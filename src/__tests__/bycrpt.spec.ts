import { BcryptStrategy, Options } from "../bycrpt";

describe("bycrpt", () => {
  const password1 = "hunter1";
  const password2 = "123456";

  const options: Options[] = [{}, { saltRounds: 1 }, { saltRounds: 5 }];

  test.each(options)("comparePassword", async opt => {
    const bcrypt = new BcryptStrategy(opt);
    const hash = await bcrypt.hashPassword(password1);
    const result1 = await bcrypt.comparePassword(password1, hash);
    expect(result1).toBe(true);
    const result2 = await bcrypt.comparePassword(password2, hash);
    expect(result2).toBe(false);
  });

  test.each(options)("comparePasswordSync", opt => {
    const bcrypt = new BcryptStrategy(opt);
    const hash = bcrypt.hashPasswordSync(password1);
    const result1 = bcrypt.comparePasswordSync(password1, hash);
    expect(result1).toBe(true);
    const result2 = bcrypt.comparePasswordSync(password2, hash);
    expect(result2).toBe(false);
  });

  test("options", () => {
    expect(() => new BcryptStrategy({ saltRounds: -1 })).toThrow();
    expect(() => new BcryptStrategy({ saltRounds: "a" } as any)).toThrow();
  });
});
