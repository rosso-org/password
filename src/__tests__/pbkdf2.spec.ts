import { Options, PBKDF2Strategy } from "../pbkdf2";

describe("pbkdf2", () => {
  const password1 = "hunter1";
  const password2 = "123456";

  const options: Options[] = [{}, { digest: "sha512" }];

  test.each(options)("comparePassword", async opt => {
    const pbkdf2 = new PBKDF2Strategy(opt);
    const hash = await pbkdf2.hashPassword(password1);
    const result1 = await pbkdf2.comparePassword(password1, hash);
    expect(result1).toBe(true);
    const result2 = await pbkdf2.comparePassword(password2, hash);
    expect(result2).toBe(false);
  });

  test.each(options)("comparePasswordSync", opt => {
    const pbkdf2 = new PBKDF2Strategy(opt);
    const hash = pbkdf2.hashPasswordSync(password1);
    const result1 = pbkdf2.comparePasswordSync(password1, hash);
    expect(result1).toBe(true);
    const result2 = pbkdf2.comparePasswordSync(password2, hash);
    expect(result2).toBe(false);
  });

  test("options", () => {
    expect(() => new PBKDF2Strategy({ saltBytes: -1 })).toThrow();
    expect(() => new PBKDF2Strategy({ hashBytes: -1 })).toThrow();
    expect(() => new PBKDF2Strategy({ iterations: -1 })).toThrow();
  });
});
