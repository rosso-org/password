# @rossoorg/password

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![Coverage][coverage_badge]][pipelines] [![NPM][npm_badge]][npm]

Password hashing implementation for Rosso.

[license]: https://gitlab.com/rosso-org/password/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/rosso-org/password/pipelines
[pipelines_badge]: https://gitlab.com/rosso-org/password/badges/master/pipeline.svg
[coverage_badge]: https://gitlab.com/rosso-org/password/badges/master/coverage.svg
[npm]: https://www.npmjs.com/package/@rossoorg/password
[npm_badge]: https://img.shields.io/npm/v/@rossoorg/password/latest.svg
