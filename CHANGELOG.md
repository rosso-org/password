# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.0.1]

### Added

- Default options of strategies

## [2.0.0]

### Changed

- Rename classes.

### Removed

- Remove `getPasswordAlgorithm`.

## 1.0.0

### Added

- Initial Release.

[unreleased]: https://gitlab.com/rosso-org/password/compare/2.0.1...master
[2.0.1]: https://gitlab.com/rosso-org/password/compare/2.0.0...2.0.1
[2.0.0]: https://gitlab.com/rosso-org/password/compare/1.0.0...2.0.0
